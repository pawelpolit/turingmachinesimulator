# __author__ = 'Pawel Polit'


def load_input():
    initial_state = raw_input('Enter the initial state:\n')
    final_states = raw_input('Enter the final states:\n')

    final_states = final_states.split(' ')

    transition_function = {}
    print 'Enter the transition function:'

    function_part = raw_input()
    while len(function_part) != 0:
        function_part = function_part.split(' ')
        transition_function[str(function_part[:2])] = function_part[2:]
        function_part = raw_input()

    memory_tape = raw_input('Enter the memory tape:\n')
    memory_tape = list(memory_tape)

    return initial_state, final_states, transition_function, memory_tape


def print_tape(memory_tape, position_on_tape):
    print (2 * position_on_tape * ' ') + '|-|' + (2 * (len(memory_tape) - position_on_tape - 1) - 1) * ' '

    tape_representation = ''
    for i in range(len(memory_tape)):
        if i == position_on_tape or i == position_on_tape + 1:
            tape_representation += '|'
        else:
            tape_representation += ' '
        tape_representation += memory_tape[i]

    if position_on_tape == len(memory_tape) - 1:
        tape_representation += '|'

    print tape_representation

    print (2 * position_on_tape * ' ') + '|-|' + (2 * (len(memory_tape) - position_on_tape - 1) - 1) * ' '


def print_info(current_state, memory_tape, position_on_tape, transition_function, final_states):
    print_tape(memory_tape, position_on_tape)
    print 'Current state: ' + current_state

    if current_state not in final_states:
        transition_function_key = str([current_state, memory_tape[position_on_tape]])
        if transition_function_key not in transition_function:
            print 'ERROR'
            exit(1)

        print 'Operation: ' + current_state + ' ' + memory_tape[position_on_tape] + ' -> ' + \
              ' '.join(transition_function[transition_function_key])
        print
        print

        return transition_function_key
    else:
        print 'Final state reached'


def start_simulation():
    current_state, final_states, transition_function, memory_tape = load_input()
    position_on_tape = 0

    while memory_tape[position_on_tape] == '-':
        position_on_tape += 1

    history = []

    while True:
        transition_function_key = print_info(current_state, memory_tape, position_on_tape, transition_function,
                                             final_states)
        command = raw_input()

        if command == 'back':
            if len(history) > 0:
                current_state, position_on_tape, memory_tape = history.pop()
            else:
                print 'History is empty'
        else:
            if current_state in final_states:
                break

            history.append([current_state, position_on_tape, memory_tape[:]])
            current_state, new_value, shift = transition_function[transition_function_key]
            memory_tape[position_on_tape] = new_value

            if shift == 'L':
                position_on_tape -= 1
            elif shift == 'R':
                position_on_tape += 1
            elif shift != 'S':
                print 'ERROR'
                exit(1)

            if position_on_tape < 0:
                memory_tape.insert(0, '-')
                position_on_tape = 0
            elif position_on_tape == len(memory_tape):
                memory_tape.append('-')


start_simulation()
